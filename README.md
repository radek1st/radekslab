# Caution! This is still work in progress!#

# Portal Development Environment #

## Clone this project and cd into it ##

## Linux ##
Note that if you are on Linux, you need to prepend the commands with **sudo** or: 
```
#!linux
sudo groupadd docker
sudo gpasswd -a ${USER} docker
# and restart docker. E.g. on Ubuntu:
sudo service docker.io restart
```

## MacOS ##

Install and run boot2docker: 
https://docs.docker.com/installation/mac/

Inside boot2docker console run this script to increase the VM memory and disk space (and open ports):
```
#!linux
./patchMacOS.sh
```

# Prepare Git checkouts #
/Users/$USER/code should contain folder PORTAL with git clones of:
```
#!linux
git@bitbucket.org:stonecutter-ondemand/cco.git
git@bitbucket.org:stonecutter-ondemand/pcao.git
git@bitbucket.org:stonecutter-ondemand/svph.git
git@bitbucket.org:stonecutter-ondemand/tbcwp.git
```

# Improved way of starting up #

Install docker-compose:
```
#!linux
curl -L https://github.com/docker/compose/releases/download/1.2.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
Startup oracle and portal with one command:
```
#!linux
docker-compose up -d
```

# Older way of starting up #
## Run Oracle Image##
You can run a clean Oracle image:
```
#!linux
./runOracleImage.sh
```
OR use one from the Repo with pre-populated data:
```
#!linux
./runOracleImageFromRepo.sh
```
## Run Portal Image##
Depends if you want to build it yourself or use the one from repository. If you want to build it, you have to execute this command:
```
#!linux
docker build -t tescobank/portal-base .
```

Run the command below if you want to run a portal from the image you built:
```
#!linux
./runPortalImage.sh
```
OR just run a portal image from the Repo:
```
#!linux
./runPortalImageFromRepo.sh
```
# Connect to Portal terminal #
Check Portal id with:
```
#!linux
docker ps
```
and connect to it:
```
#!linux
docker exec -it <portal-container-id> bash
```
If you see error messages related to the terminal run this:
```
#!linux
export TERM=xterm
```

## Build Portal (optional) ##
If you built the image yourself, you need to build the portal too:
```
#!linux
./build-tbcwp.sh
./build-ccola.sh
./build-svph.sh
```
## Start JBoss ##
```
#!linux
./start-jboss.sh
```
The logs go to nohup.out, check there for any errors
## Check if it's running ##
http://localhost:8080/creditcards/example-product-page.page

## If it's working fine and you built the images yourself rather then using the registry or docker-compose, you could save them to spare yourself building everything all over again ##
```
#!linux
docker commit <container_id> tescobank/portal
docker commit <container_id> tescobank/oracle
```