FROM ubuntu:15.04

MAINTAINER Radek Ostrowski <radoslaw.ostrowski@i.tescobank.com>

# Install Java 6, Git, Maven, Curl, Nano, Telnet
RUN apt-get update && apt-get install -y python-software-properties software-properties-common \
  && add-apt-repository ppa:webupd8team/java \
  && echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
  && echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections \
  && apt-get update && apt-get -y install oracle-java6-installer git maven curl nano telnet 

# Create and setup default user
ENV HOME /home/developer
RUN useradd -ms /bin/bash developer 
COPY resources/settings.xml /home/developer/.m2/settings.xml


# Portal stuff
RUN mkdir /portal
COPY resources/keystores/* /portal/keystores/
RUN chown -R developer:developer /portal && chmod -R 755 /portal/keystores
COPY resources/build-ccola.sh /home/developer/
COPY resources/install-jboss.sh /home/developer/
COPY resources/start-jboss.sh /home/developer/
COPY resources/build-stubs-tbcwp.sh /home/developer/
COPY resources/stop-jboss.sh /home/developer/
COPY resources/build-tbcwp.sh /home/developer/
COPY resources/build-pcao.sh /home/developer/
COPY resources/build-svph.sh /home/developer/

RUN chmod +x /home/developer/*.sh

# Volume map point for Git repos
RUN mkdir /home/developer/code

# Become developer
RUN chown -R developer:developer /home/developer
USER developer
WORKDIR /home/developer

ENV TZ Europe/London
EXPOSE 8080 8443 8787


