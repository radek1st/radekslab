export DB_CONNECTION=jdbc:oracle:thin:@$ORACLE_PORT_1521_TCP_ADDR:$ORACLE_PORT_1521_TCP_PORT:xe

cd /home/developer/code/PORTAL/tbcwp/platform-deploy/environment-config
mvn clean install -Dmaven.test.skip=true -Ptb -Dsss.db.url=$DB_CONNECTION -Dpcao.db.url=$DB_CONNECTION -Doa.db.url=$DB_CONNECTION -Dportal.db.url=$DB_CONNECTION -Dtbmb.db.url=$DB_CONNECTION

