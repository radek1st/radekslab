docker run --link oracle:oracle \
  -p 8080:8080 -p 8443:8443 -p 8787:8787 \
  -v /Users/$USER/code:/home/developer/code \
  -v /Users/$USER/.m2/repository:/home/developer/.m2/repository \
  --add-host nexus.tbdevops.com:54.194.70.81 \
  -d ci61.tbdevops.com:8180/tescobank/portal tail -F /etc/hosts
