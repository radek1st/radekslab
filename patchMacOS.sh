printf "DiskSize = 50000\nMemory = 8192" >> ~/.boot2docker/profile
boot2docker stop
boot2docker destroy
boot2docker init
boot2docker start
boot2docker ssh
sudo sh
echo 'EXTRA_ARGS="--insecure-registry ci61.tbdevops.com:8180"' >> /var/lib/boot2docker/profile
exit
exit
boot2docker restart

VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port1521,tcp,,1521,,1521";
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port8080,tcp,,8080,,8080";
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port8787,tcp,,8787,,8787";
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port8443,tcp,,8443,,8443";

# for loans
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port9510,tcp,,9510,,9510";
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port9540,tcp,,9540,,9540";
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port9550,tcp,,9550,,9550";
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port9560,tcp,,9560,,9560";
VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port9570,tcp,,9570,,9570";

